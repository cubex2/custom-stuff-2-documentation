import os, os.path, sys
import shutil

docs_dir = os.path.dirname(os.path.abspath(__file__))
src_dir = os.path.join(docs_dir, 'src')
bin_dir = os.path.join(docs_dir, 'bin')

def getScriptingTable(scriptObjects, triggers, usedSObjects, name):
    table = '<tr>'
    table += '<td></td>'
    for scriptObject in scriptObjects:
        if scriptObject.split('|')[0] in usedSObjects:
            if "|" in scriptObject and not "NULL" in scriptObject:
                scriptObjectName = scriptObject.split('|')[0]
                scriptObjectUrl = scriptObject.split('|')[1]
                table += '<th><a href="javadoc/cubex2/cs2/scripting/' + scriptObjectUrl + '.html">' + scriptObjectName + '</a></th>'
            else:
                table += '<th>' + scriptObject.split('|')[0] + '</th>'
    table += '</tr>'
        
    for trigger in triggers:
        table += '<tr><th><a href="' + name + '/' + trigger.split('|')[0] + '.html">' + trigger.split('|')[0] + '</a></th>'
        for scriptObject in scriptObjects:
            if scriptObject.split('|')[0] in usedSObjects:
                table += '<td>'
                if scriptObject.split('|')[0] in trigger.split('|')[1:]:
                    table += 'X'
                table += '</td>'
        table += "</tr>"
    return table

if os.path.exists(bin_dir):
    shutil.rmtree(bin_dir)
    
os.mkdir(bin_dir)
os.mkdir('bin/block')
os.mkdir('bin/gui')
os.mkdir('bin/item')
os.mkdir('bin/tileEntity')
os.mkdir('bin/worldGen')
os.mkdir('bin/tutorial')

shutil.copytree('src/images', 'bin/images')
shutil.copytree('src/examples/textures', 'bin/examples/textures')
shutil.copytree('src/javadoc', 'bin/javadoc')
shutil.copytree('src/tutorial/textures', 'bin/tutorial/textures')
shutil.copy2('src/stylesheet.css', 'bin/stylesheet.css')

f = open('src/naviList.txt', 'r')
naviList = f.readlines()
f.close()

f = open('src/pages.txt', 'r')
pages = f.readlines()
f.close()

f = open('src/body.html')
htmlBody = f.read()
f.close()

for page in pages:
    if page.endswith('\n'):
        page = page[0:-1]
    navi = ''
    for entry in naviList:        
        href = page.count('/') * '../' + entry.split('|')[0] + '.html'
        navi = navi + '<li><a class="navi" href="' + href + '">' + entry.split('|')[1] + '</a></li>\r\n'

    f = open('src/' + page, 'r')
    pageContent = f.read()
    f.close()

    cssPath = page.count('/') * '../' + 'stylesheet.css'

    pageOutput = htmlBody.replace('<replace naviList />', navi)
    pageOutput = pageOutput.replace('<replace css />', cssPath)
    pageOutput = pageOutput.replace('<replace pageContent />', pageContent)

    if page == 'scripting.html':
        f = open('src/scriptObjects.txt', 'r')
        scriptObjects = f.readlines();
        f.close()

        f = open('src/blockTriggers.txt', 'r')
        lines = f.readlines()
        blockObjects = lines[0].split('|')
        blockTriggers = lines[1:]
        f.close()

        f = open('src/itemTriggers.txt', 'r')
        lines = f.readlines()
        itemObjects = lines[0].split('|')
        itemTriggers = lines[1:]
        f.close()

        f = open('src/tileEntityTriggers.txt', 'r')
        lines = f.readlines()
        teObjects = lines[0].split('|')
        teTriggers = lines[1:]
        f.close()

        f = open('src/guiTriggers.txt', 'r')
        lines = f.readlines()
        guiObjects = lines[0].split('|')
        guiTriggers = lines[1:]
        f.close()

        blockTable = getScriptingTable(scriptObjects, blockTriggers, blockObjects, 'block')
        itemTable = getScriptingTable(scriptObjects, itemTriggers, itemObjects, 'item')
        teTable = getScriptingTable(scriptObjects, teTriggers, teObjects, 'tileEntity')
        guiTable = getScriptingTable(scriptObjects, guiTriggers, guiObjects, 'gui')

        pageOutput = pageOutput.replace('<replace blocktable/>', blockTable)
        pageOutput = pageOutput.replace('<replace itemtable/>', itemTable)
        pageOutput = pageOutput.replace('<replace tileentitytable/>', teTable)
        pageOutput = pageOutput.replace('<replace guitable/>', guiTable)
        

    f = open('bin/' + page, 'w')
    f.write(pageOutput)
    f.close()


                                    
